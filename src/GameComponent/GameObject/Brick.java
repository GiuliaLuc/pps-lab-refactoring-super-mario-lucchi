package GameComponent.GameObject;

public class Brick extends GameObjectImpl {
	
	public Brick(int x, int y) {
		super(x, y, 30 ,30, "/res/images/Brick.png)");
	}
}
