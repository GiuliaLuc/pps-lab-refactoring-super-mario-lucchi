package GameComponent.GameObject;

import GameComponent.GameComponent;

import javax.swing.*;
import java.awt.*;

/**
 * Created by lucch on 28/02/2017.
 */
public interface GameObject extends GameComponent {

    Image getImage();
    ImageIcon getIcon();

    void setIcon(ImageIcon icon);
    void setImage(Image image);
}
