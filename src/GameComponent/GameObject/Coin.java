package GameComponent.GameObject;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Coin extends GameObjectImpl implements Runnable{

	private int counter;
	private final int PAUSE = 10;
	public Coin(int x, int y) {
		super(x, y, 30 ,30, "/res/imagine/piece1.png");
	}
	
	public Image move(){
		
		ImageIcon icon ;
		Image image;
		String str ;
		this.counter++;
		if(this.counter / 100 == 0){
			str= "/res/imagine/piece1.png";
		}else{
		    str = "/res/imagine/piec.png";
        }
		if(this.counter == 200 ) {this.counter = 0;}
		icon = new ImageIcon(getClass().getResource(str));
		image =icon.getImage();

		return image;
	}

	@Override
	public void run() {
		
		try{
		    Thread.sleep(10);
		}catch(InterruptedException e){

        }
			
		while(true){
			this.move();
			try{
			    Thread.sleep(PAUSE);
			}catch(InterruptedException e){

            }
		}
		
	}

}
