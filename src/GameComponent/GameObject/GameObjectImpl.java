package GameComponent.GameObject;

import java.awt.Image;

import javax.swing.ImageIcon;

import Game.Main;

public class GameObjectImpl implements GameObject {
	
	private double width;
	private double height;
	private double x;
	private double y;
	
	private Image image ;
	private ImageIcon icon;
	
	public GameObjectImpl(final double x, final double y, final double width, final double height, final String pathImage){
			
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height ;
		setIcon(new ImageIcon(getClass().getResource(pathImage)));
		this.icon = getIcon();
		setImage(icon.getImage());
	}


	@Override
	public double getWidth(){
		return width;
	}

	@Override
	public double getHeight(){
		return height;
	}

	@Override
	public double getX(){
		return x;
	}

	@Override
	public double getY(){
		return y;
	}

	@Override
	public Image getImage(){
		return image;
	}

	@Override
	public ImageIcon getIcon(){
		return icon;
	}


	@Override
	public void setWidth(double l){
		this.width = l;
	}

	@Override
	public void setHeight(double height){
		this.height = height;
	}

	@Override
	public void setX(double x){
		this.x = x;
	}

	@Override
	public void setY(double y){
		this.y = y;
	}

	@Override
	public void setIcon(ImageIcon icon) {
		this.icon= icon;
	}

	@Override
	public void setImage(Image image) {
		this.image= image;
	}

	@Override
	public void displacement(){
		
		if(Main.scene.getxPos() >= 0){
			this.x = this.x - Main.scene.getMov();
		}
	}
	
}
