package GameComponent;

/**
 * Created by lucch on 01/03/2017.
 */
public interface GameComponent {

    double getWidth();
    double getHeight();
    double getX();
    double getY();

    void setWidth(double width);
    void setHeight(double height);
    void setX(double x);
    void setY(double y);

    void displacement();
}
