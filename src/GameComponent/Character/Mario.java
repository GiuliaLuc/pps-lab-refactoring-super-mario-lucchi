package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

import Game.Main;
import GameComponent.GameObject.GameObjectImpl;
import GameComponent.GameObject.Coin;

public class Mario extends CharacterImpl {

	private ImageIcon imageIcon;
	private Image image;
	private boolean isJumping;
	private int intensityJump;
	
	public Mario(int X, int Y) {
		super(X, Y, 28 , 50);
		imageIcon = new ImageIcon(getClass().getResource("/res/imagine/marioD.png"));
		this.image = this.imageIcon.getImage();
		this.isJumping = false;
		this.intensityJump = 0;
	}

	public boolean isJumping() {
		return isJumping;
	}

	public void setJumping(boolean jumping) {
		this.isJumping = jumping;
	}

	public Image getImage() {
		return image;
	}

	@Override
	public Image characterMovement(String name , int frequency ){
		
		String pathImage ;
		ImageIcon icon ;
		Image image ;
		
		if(this.getMoving() == false || Main.scene.getxPos() <=0 || Main.scene.getxPos() > 4600){

			if(this.getMovingRight() == true){
				pathImage = "/res/imagine/" + name + "AD.png";
			}else pathImage = "/res/imagine/" + name + "AG.png"; //se non
		}else {
			this.setCounter(this.getCounter()+1);
			if(this.getCounter() / frequency == 0){ //
				if(this.getMovingRight() == true){
					pathImage = "/res/imagine/" + name + "AD.png";
				}else pathImage = "/res/imagine/" + name + "AG.png";
			}else{
				if(this.getMovingRight() == true){
					pathImage = "/res/imagine/" + name + "D.png";
				}else pathImage = "/res/imagine/" + name + "G.png";
			}
			if(this.getCounter() ==2*frequency) {
                this.setCounter(0);
            }
		}

		icon = new ImageIcon(getClass().getResource(pathImage));
		image = icon.getImage();
		
		return image;
	
	}
	
	public Image Jumping(){
		
		ImageIcon icon ;
		Image image;
		String pathImage ;
		
		this.intensityJump++;
		
		if(this.intensityJump <= 41){
			if(this.getY() > Main.scene.getHauteurPlafond())
				this.setY(this.getY() - 4);
			else this.intensityJump = 42 ;
			if(this.getMovingRight() == true)
				pathImage = "/res/imagine/marioSD.png";
			else pathImage = "/res/imagine/marioSG.png";
			
			//discesa 
		}else if (this.getY() + this.getHeight() < Main.scene.getySol()){
			this.setY(this.getY() + 1);
			if(this.getMovingRight()== true)
				pathImage = "/res/imagine/marioSD.png";
			else pathImage = "/res/imagine/marioSG.png";
			
			// isJumping finito
		}else {
			if(this.getMovingRight() == true)
				pathImage = "/res/imagine/marioAD.png";
			else pathImage = "/res/imagine/marioAG.png";
			this.isJumping = false;
			this.intensityJump = 0 ;
		}
		//reinitialisazione img mario
		icon = new ImageIcon(getClass().getResource(pathImage));
		image = icon.getImage();
		return image ;
				
	}
	
	public void contact (GameObjectImpl object){
		if(this.isForwardMovement(object)== true && this.getMovingRight()==true ||
				 (this.isBackMovement(object)==true)&& this.getMovingRight()== false ){
			Main.scene.setMov(0);
			this.setMoving(false);
		}

		if(this.isMoveDown(object)==true && this.isJumping == true) {
			Main.scene.setySol((int)object.getX());
		}else if (this.isMoveDown(object)==false){
			Main.scene.setySol(293);
			if(this.isJumping == false){this.setY(243);
		}
		
		if(this.isMoveUp(object) == true){
			Main.scene.setHauteurPlafond((int)object.getY() + (int)object.getHeight());
		}else if (this.isMoveUp(object) == false && this.isJumping == false){
			Main.scene.setHauteurPlafond(0);
		}	
		}
	}
	
	public boolean contactPiece(Coin piece){
		if(this.isMoveDown(piece) == true || this.isMoveUp(piece) == true || this.isForwardMovement(piece)==true
                ||this.isMoveDown(piece) == true){
			return true;
		}else{ return false;}
	}
	
	public void contact(CharacterImpl character){
		if((this.isForwardMovement(character) == true) || (this.isBackMovement(character) == true)){
			if(character.getAlive() == true){
				this.setMoving(false);
		    	this.setAlive(false);
			}
			else this.setAlive(true);
		}else if(this.isMoveDown(character) == true){
			character.setMoving(false);
			character.setAlive(false);
		}
	}
}
