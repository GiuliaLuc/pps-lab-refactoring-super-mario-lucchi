package GameComponent.Character;

import GameComponent.GameComponent;
import GameComponent.GameObject.GameObjectImpl;

import java.awt.Image;

public interface Character extends GameComponent {

    boolean getAlive();

    boolean getMoving();

    boolean getMovingRight();

    int getCounter();


    void setAlive(boolean alive);

    void setMoving(boolean moving);

    void setMovingRight(boolean movingRight);

    void setCounter(int counter);


    Image characterMovement(String name , int frequency );

    boolean isForwardMovement(GameObjectImpl og);

    boolean isBackMovement(GameObjectImpl object);

    boolean isMoveDown(GameObjectImpl object);

    boolean isMoveUp(GameObjectImpl og);

    boolean isNearObject(GameObjectImpl obj);

    boolean isForwardMovement(CharacterImpl character);

    boolean isBackMovement(CharacterImpl character);

    boolean isMoveDown(CharacterImpl character);

    boolean isNearFromCharacter(CharacterImpl character);
}
