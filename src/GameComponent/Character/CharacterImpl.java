package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

import Game.Main;
import GameComponent.GameObject.GameObjectImpl;

public class CharacterImpl implements Character{
	
	private double width;
    private double height;
	private double x;
    private double y;
	private boolean isMoving;
	private boolean isMovingRight;
	private int counter;
	private boolean isAlive;
	
	
	public CharacterImpl(final double x , final double y , final double width , final double height){
		
		this.x = x ;
		this.y = y ;
		this.height = width ;
		this.width = height ;
		this.counter = 0;
		this.isMoving = false ;
		this.isMovingRight = true ;
		this.isAlive = true;
	}

    @Override
	public boolean getAlive() {
		return isAlive;
	}
	@Override
	public double getWidth() {
		return width;
	}
	@Override
	public double getHeight() {
		return height;
	}
	@Override
	public double getX() {
		return x;
	}
    @Override
	public double getY() {
		return y;
	}
	@Override
	public boolean getMoving() {
		return isMoving;
	}
	@Override
	public boolean getMovingRight() {
		return isMovingRight;
	}
	@Override
	public int getCounter() {
		return counter;
	}

	@Override
	public void setAlive(boolean alive) {
		this.isAlive = alive;
	}
	@Override
	public void setX(double x) {
		this.x = x;
	}
	@Override
	public void setY(double y) {
		this.y = y;
	}
    @Override
    public void setWidth(double width) {
        this.width=width;
    }
    @Override
    public void setHeight(double height) {
        this.height= height;
    }
	@Override
	public void setMoving(boolean moving) {
		this.isMoving = moving;
	}
	@Override
	public void setMovingRight(boolean movingRight) {
		this.isMovingRight = movingRight;
	}
	@Override
	public void setCounter(int counter) {
		this.counter = counter;
    }
	
	//metodo per gestire i movimenti dei personagi

    @Override
	public Image characterMovement(String name , int frequency ){

		String pathImage ;
		ImageIcon icon ;
		Image image ;

		if(this.isMoving == false ){

			if(this.isMovingRight == true){ // se guarda a destra
				pathImage = "/res/imagine/" + name + "AD.png";
			}else{
			    pathImage = "/res/imagine/" + name + "AG.png"; //se non
            }
		}else {
			this.counter++;
			if(this.counter / frequency == 0){ //
				if(this.isMovingRight == true){
					pathImage = "/res/imagine/" + name + "AD.png"; // mario fermo a destra
				}else{
				    pathImage = "/res/imagine/" + name + "AG.png";// mario fermo a sinistra
                }
			}else{
				if(this.isMovingRight == true){
					pathImage = "/res/imagine/" + name + "D.png"; // mario che camina verso destra
				}else{
				    pathImage = "/res/imagine/" + name + "G.png";// mario che camina verso sinistra
                }
			}
			if(this.counter == 2* frequency ) {
                this.counter = 0;
            }
		}

		icon = new ImageIcon(getClass().getResource(pathImage));
		image = icon.getImage();

		return image;
	}

	@Override
	public void displacement (){
		if(Main.scene.getxPos() >= 0){
			this.x = this.x - Main.scene.getMov();
		}
	}

	@Override
	public boolean isForwardMovement(GameObjectImpl object) {
        if (this.x + this.width < object.getX() || this.x + this.width > object.getX() + 5 ||
                this.y + this.height <= object.getY() || this.y >= object.getY() + object.getHeight()) {
            return false;
        } else{
            return true;
        }
	}
	
	@Override
    public boolean isBackMovement(GameObjectImpl object){
		if(this.x > object.getX() + object.getWidth() ||this.x + this.width < object.getX() + object.getWidth() -5 ||
			this.y + this.height <= object.getY() || this.y >= object.getY() + object.getHeight() ){
			return false;
		}else{
			return true;
		}
	}
	
    @Override
    public boolean isMoveDown(GameObjectImpl object){
		if(this.x + this.width < object.getX() + 5 || this.x  > object.getX() + object.getWidth() - 5 ||
			this.y + this.height < object.getY() || this.y +this.height > object.getY() + 5 ){
			return false;
		}else{
            return true;
        }

	}
	
	@Override
    public boolean isMoveUp(GameObjectImpl object){
		if(this.x + this.width < object.getX() + 5 || this.x  > object.getX() + object.getWidth() - 5 ||
			this.y < object.getY() + object.getHeight() || this.y > object.getY()+ object.getHeight() +5 ) {
			return false;
		}else{
		    return true;
        }
	}

	@Override
	public boolean isNearObject(GameObjectImpl object){
		
		if((this.x > object.getX() - 10 && this.x < object.getX() + object.getWidth() + 10) ||
			(this.getX()+ this.width > object.getX() - 10 && this.x + this.width < object.getX() + object.getWidth() + 10)) {
				return true;
		}else{
		    return false ;
		}
		
	}

	@Override
    public boolean isForwardMovement(CharacterImpl character){
		if(this.getMovingRight() == true){
			if(this.x + this.width < character.getX() || this.x + this.width > character.getX() + 5 ||
					this.y + this.height <= character.getY() || this.y >= character.getY() + character.getHeight()){
				return false;
				}
			else{return true;}
		}else{return false;}
	}

    @Override
    public boolean isBackMovement(CharacterImpl character){
		if(this.x > character.getX() + character.getWidth() || this.x + this.width < character.getX() + character.getWidth() - 5 ||
				this.y + this.height <= character.getY() || this.y >= character.getY() +character.getHeight()){
			return false;
			}
		else{return true;}
	}

	@Override
	public boolean isMoveDown(CharacterImpl character){
		if(this.x + this.width < character.getX() || this.x > character.getX() + character.getWidth() ||
				this.y + this.height < character.getY() || this.y + this.height > character.getY()){
			return false;
		}
		else{return true;}
	}

	@Override
	public boolean isNearFromCharacter(CharacterImpl character){
		if((this.x > character.getX() - 10 && this.x < character.getX() + character.getWidth() + 10)
		    	|| (this.x + this.width > character.getX() - 10 && this.x + this.width < character.getX() +character.getWidth() + 10)){
			return true;
			}
		    	else{return false;}
	}
}
