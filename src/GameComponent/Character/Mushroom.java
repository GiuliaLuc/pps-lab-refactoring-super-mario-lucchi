package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

import GameComponent.GameObject.GameObjectImpl;

public class Mushroom extends CharacterImpl implements Runnable{

	private Image mushroomImage;
	private ImageIcon iconMushroom;

	private final int PAUSE = 15;
	private int dxFunghi;

	public Mushroom(int X, int Y) {
		super(X, Y,27 , 30);
		super.setMovingRight(true);
		super.setMoving(true);
		this.dxFunghi = 1;
		this.iconMushroom = new ImageIcon(getClass().getResource("/res/imagine/funghiAD.png"));
		this.mushroomImage = iconMushroom.getImage();

		Thread mushroomThread = new Thread(this);
		mushroomThread.start();
	}

	public Image getMushroomImage() {
		return mushroomImage;
	}


	public void displecement(){
		if(super.getMovingRight() == true){this.dxFunghi =1;}
		else{this.dxFunghi= -1; }
		super.setX(super.getX()+this.dxFunghi);

	}

	@Override
	public void run() {
		try{Thread.sleep(20);}
		catch(InterruptedException e){}

		while(true){
			if(super.getAlive() == true){
				this.displecement();
				try{Thread.sleep(PAUSE);}
				catch(InterruptedException e){}
			}
		}
	}

	public void contactWithObject(GameObjectImpl object){
		if(this.isForwardMovement(object) == true && this.getMovingRight() == true){
			super.setMovingRight(false);
			this.dxFunghi = -1;
		}else if (this.isBackMovement(object)==true && this.getMovingRight() ==false){
			super.setMovingRight(true);
			this.dxFunghi = 1;
		}
	}

	public void contactWithCharacter(CharacterImpl character){
        if(this.isForwardMovement(character) == true && this.getMovingRight() == true){
            super.setMovingRight(false);
            this.dxFunghi = -1;
        }else if (this.isBackMovement(character)==true && this.getMovingRight() ==false){
            super.setMovingRight(true);
            this.dxFunghi = 1;
        }
	}
	

	public Image ChangeImageAfterDeath(){
			String pathImage;
			ImageIcon imageIcon;
			Image image;
	        if(this.getMovingRight() == true){pathImage = "/res/imagine/funghiED.png";}
	        else{pathImage = "/res/imagine/funghiEG.png";}
	        imageIcon = new ImageIcon(getClass().getResource(pathImage));
	        image = imageIcon.getImage();
			return image;
	}

}
